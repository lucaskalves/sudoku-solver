package sudoku_solver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class InstanceFileLoader {

	private FileReader instanceFile;
	private Instance instance;

	/**
	 * Constructs a Instance Generator that will read an instance file
	 * 
	 * @param filepath
	 *            The instance file path
	 * @throws IOException 
	 * 
	 */
	public InstanceFileLoader(String filepath) throws IOException {
		instanceFile = new FileReader(filepath);

		makeInstance();
	}

	private void makeInstance() throws IOException {

		BufferedReader buffer = new BufferedReader(instanceFile);

			String firstLine = buffer.readLine();
			int instanceSize = Integer.parseInt(firstLine);
			instance = new Instance(instanceSize);

			String line;
			int x = 0;
			while ((line = buffer.readLine()) != null)
			{
				int y = 0;
				
				String[] values = line.split("\\s+");
				for (String strCell : values) {
					
					int intCell = Integer.parseInt(strCell);
					if(intCell > 0) {
						instance.setFixedCell(x, y, intCell);
					} else {
						instance.setFreeCell(x, y, intCell);
					}
					y += 1;
				}
				x += 1;
			}
			instanceFile.close();
	}

	public Instance getInstance() {
		return instance;
	}
}