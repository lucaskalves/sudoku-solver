package sudoku_solver;

/**
 * Represents a cell of the Sudoku board.
 */
public class Cell {
	int value = -1;
	boolean isFixed;

	/**
	 * Constructs a cell with a value
	 * 
	 * @param aValue
	 *            The value of the cell
	 * @param aIsFixed
	 *            Sets if the value can change
	 */
	public Cell(int aValue, boolean aIsFixed) {
		value = aValue;
		isFixed = aIsFixed;
	}

	/**
	 * Constructs an empty cell
	 */
	public Cell() {
		value = -1;
		isFixed = false;
	}

	/**
	 * Set the value of free cells. it the cell is fixed, it will throw a
	 * runtime exception.
	 * 
	 * @param aValue
	 *            The new value of the cell
	 */
	public void setValue(int aValue) {
		if (isFixed) {
			throw new RuntimeException(
					"Tried to change the value of a fixed cell.");
		}

		value = aValue;
	}

	public void fix() {
		isFixed = true;
	}

	public int getValue() {
		return value;
	}

	public boolean isFixed() {
		return isFixed;
	}

	public boolean isEmpty() {
		return value <= 0;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (!(other instanceof Cell))
			return false;

		Cell otherCell = (Cell) other;

		return otherCell.getValue() == value
				&& otherCell.isEmpty() == isEmpty()
				&& otherCell.isFixed() == isFixed;
	}

	@Override
	public int hashCode() {
		int result = 13;
		int c = (isFixed ? 1 : 0) + value;
		result = 37 * result + c;
		
		return result;
	}
}
