package sudoku_solver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Implementation of the GRASP (Greedy Randomized Adaptive Search Procedure)
 * algorithm.
 * 
 * http://en.wikipedia.org/wiki/Greedy_randomized_adaptive_search_procedure
 */
public class GRASP {
	int localSearchMaxIterations;
	int maxIterations;
	Instance initialInstance;
	float alpha;
	Set<Integer> hashtable = new HashSet<>();
	Set<Integer> localSearchHashtable = new HashSet<>();

	/**
	 * Mounts a GRASP solver.
	 * 
	 * @param aMaxIterationsNumber
	 *            The limit number of iterations of the grasp algorithm. If this
	 *            number is negative, GRASP will run until your CPU explodes (or
	 *            it find a solution).
	 * @param anInitialInstance
	 *            The initial valid instance of sudoku.
	 * @param anAlpha
	 *            The coefficient of randomization.
	 * @param aMaxLocalSearchIterationsNumber
	 *            The limit number of iterations of the local search algorithm.
	 *            If this number is negative, it will run until it face the
	 *            solution or a worse solution (minimumn local).
	 * @throws IllegalArgumentException
	 */
	public GRASP(int aMaxIterationsNumber, Instance anInitialInstance,
			float anAlpha, int aMaxLocalSearchIterationsNumber)
			throws IllegalArgumentException {

		if (!anInitialInstance.isValid()) {
			throw new IllegalArgumentException(
					"The passed initial instance is invalid.");
		}
		if (anAlpha < 0.0f || anAlpha > 1.0f) {
			throw new IllegalArgumentException(
					"Alpha value should be >= 0.0 and <= 1.0");
		}

		maxIterations = aMaxIterationsNumber;
		initialInstance = anInitialInstance;
		alpha = anAlpha;
		localSearchMaxIterations = aMaxLocalSearchIterationsNumber;
	}

	/**
	 * Runs the algorithm to find a solution.
	 * 
	 * @return The best solution the algorithm found.
	 */
	public Instance execute() {
		Instance s = initialInstance;
		Instance s_ = null;
		int remainingIterations = maxIterations;
		long runnedIterations = 0;

		System.out.printf(
				"Started GRASP with:\n" + " \talpha: %f\n"
						+ "\tgrasp-max-iterations: %s\n"
						+ "\tlocal-search-max-iterations: %s\n",
				alpha,
				(maxIterations >= 0) ? String.valueOf(maxIterations)
						: "unlimited",
				(localSearchMaxIterations >= 0) ? String
						.valueOf(localSearchMaxIterations) : "unlimited");

		while (remainingIterations != 0 && !s.isSolved()) {
			if (runnedIterations % 10 == 0) {
				System.out.printf("%d) optimal: %d\n", runnedIterations,
						s.cost());
			}

			s_ = construct();

			if (!hashtable.contains(s_.hashCode())) {
				hashtable.add(s_.hashCode());

				s_ = localSearch(s_);

				if (s_.cost() < s.cost()) {
					s = s_;
				}
			}

			if (maxIterations >= 0) {
				remainingIterations -= 1;
			}

			runnedIterations += 1;
		}

		return s;
	}

	/**
	 * Construct a new instance greedily and randomly
	 * 
	 * @return An instance
	 */
	public Instance construct() {
		int n = initialInstance.getSize();
		Instance newInstance = initialInstance.cloneInstance(initialInstance);
		newInstance.clean();

		for (int i = 0; i < n * n; i += n) {
			for (int j = 0; j < n * n; j += n) {
				List<Integer> candidateList = new ArrayList<Integer>();
				for (int k = 1; k <= n * n; k++) {
					candidateList.add(k);
				}

				// constructs the candidate list
				for (int k = 0; k < n; k++) {
					for (int l = 0; l < n; l++) {
						int linha = k + i;
						int coluna = l + j;

						if (!newInstance.getCell(linha, coluna).isEmpty()) {
							candidateList.remove(candidateList
									.indexOf(newInstance.getCell(linha, coluna)
											.getValue()));
						}

					}
				}

				// find a new solution element and put it in the instance
				for (int k = 0; k < n; k++) {
					for (int l = 0; l < n; l++) {
						int linha = k + i;
						int coluna = l + j;
						if (newInstance.getCell(linha, coluna).isEmpty()) {

							// construct RCL
							List<Integer[]> rcl = new ArrayList<>();
							for (int m = 0; m < candidateList.size(); m++) {
								int cost = 0;
								for (int posn = 0; posn < n * n; posn++) {
									if (newInstance.getCell(linha, posn)
											.getValue() == candidateList.get(m)) {
										cost += 1;
									}
									if (newInstance.getCell(posn, coluna)
											.getValue() == candidateList.get(m)) {
										cost += 1;
									}
								}
								Integer element[] = { candidateList.get(m),
										cost };
								rcl.add(element);
							}

							Collections.sort(rcl, new Comparator<Integer[]>() {
								public int compare(Integer[] element,
										Integer[] otherElement) {
									return element[1]
											.compareTo(otherElement[1]);
								}
							});

							// randomly get one of the (alpha)% best candidates
							int maxIndex = (int) Math.ceil(alpha * rcl.size());
							int index = 0;
							if(maxIndex > 0) {
								Random random = new Random();
								index = random.nextInt(maxIndex);
							}
							int selected = rcl.get(index)[0];
							newInstance.setFreeCell(linha, coluna, selected);
							candidateList.remove(candidateList
									.indexOf(selected));
						}

					}
				}
			}
		}

		return newInstance;
	}

	/**
	 * Search locally for another solution. It is the hill climbing algorithm.
	 * 
	 * @param anInstance
	 *            The initial instance
	 * @return a new and probably better solution
	 */
	public Instance localSearch(Instance anInstance) {
		Instance current = anInstance;
		int iterations = localSearchMaxIterations;

		while (iterations != 0) {
			if (current.isSolved()
					&& !localSearchHashtable.contains(current.hashCode())) {
				return current;
			}

			Set<Instance> neighbours = current.neighbourhood();
			int nextCost = Integer.MAX_VALUE;
			Instance nextInstance = null;

			// search the neighbourhood for a better solution
			for (Instance neighbor : neighbours) {
				if (neighbor.cost() < nextCost) {
					nextInstance = neighbor;
					nextCost = neighbor.cost();
				}
			}

			if (nextCost >= current.cost()) {
				// stop when no better neighbour was found
				return current;
			}

			localSearchHashtable.add(current.hashCode());

			current = nextInstance;

			if (localSearchMaxIterations >= 0) {
				iterations--;
			}
		}

		// no better solution was found
		return current;
	}

}
