package sudoku_solver;

import java.util.Date;

/**
 * Hello world!
 */
public class App {
	public static void main(String[] args) {
		// parameters
		String instanceFilepath = null;
		int graspMaxIterations = 1000;
		float graspAlpha = 0.2f;
		int localsearchMaxIterations = 2000;

		if (args.length < 1) {
			printUsage();
			System.exit(1);
		}

		// instance filepath should be the first argument
		instanceFilepath = args[0];

		// parse the args
		if (args.length > 1) {
			int i = 1;
			while (i < args.length) {
				String item = args[i];

				try {
					switch (item) {
					case "-g":
						i++;
						if (i >= args.length) {
							throw new Exception(
									"option -g requires a number argument");
						}
						graspMaxIterations = Integer.parseInt(args[i]);
						break;
					case "-a":
						i++;
						if (i >= args.length) {
							throw new Exception(
									"option -a requires a number argument");
						}
						graspAlpha = Float.parseFloat(args[i]);
						break;
					case "-l":
						i++;
						if (i >= args.length) {
							throw new Exception(
									"option -l requires a number argument");
						}
						localsearchMaxIterations = Integer.parseInt(args[i]);
						break;
					default:
						throw new Exception("Unknow option " + item);
					}
				} catch (Exception e) {
					System.err.println("Error: " + e.getMessage());
					System.err.println();
					printUsage();
					System.exit(1);
				}

				i++;
			}
		}

		try {
			InstanceFileLoader theReader = new InstanceFileLoader(
					instanceFilepath);
			Instance initialInstance = theReader.getInstance();

			long start = new Date().getTime();
			GRASP grasp = new GRASP(graspMaxIterations, initialInstance,
					graspAlpha, localsearchMaxIterations);
			Instance solution = grasp.execute();
			long end = new Date().getTime();
			
			System.out.printf("\nElapsed time (ms): %d (%f s)\n",end-start,(float)(end-start)/1000.0);

			if (solution.isSolved()) {
				System.out.println("\nSo nice! I could solve this.");
			} else {
				System.out.println("\nOh no, that was too hard for me. :/");
			}

			// print the final grid
			System.out.println("\nFinal grid:");
			System.out.println();
			System.out.println(solution);

		} catch (Exception e) {
			System.out.println("A terrible error occurred. I'm ashamed.");
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private static void printUsage() {
		System.err
				.println("Usage: sudoku-solver <instance-filepath> [-g grasp-max-iterations]");
		System.err
				.println("                     [-a grasp-alpha] [-l local-search-max-iterations]");
		System.err.println();
		System.err.println("Options");
		System.err.println("\tinstance-filepath");
		System.err.println("\t\tfilepath to the sudoku instance file");
		System.err.println("\t-g");
		System.err
				.println("\t\tmax number of iterations GRASP will run. Default = 1000");
		System.err.println("\t-a");
		System.err.println("\t\talpha value for GRASP. Default = 0.2");
		System.err.println("\t-l");
		System.err
				.println("\t\tmax number of iterations local search will run. Default = 2000");
	}

}
