package sudoku_solver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a Sudoku Instance.
 * 
 * It holds the data in a (n^2)x(n^2) matrix. Each cell can hold integer values
 * where: < 1 represents empty cells >= 1 represents filled cells > n^2 are
 * invalid values
 */
public class Instance {
	int n;
	Cell matrix[][];

	public Instance(int aNValue) {
		if (aNValue < 2) {
			throw new RuntimeException("The minimum sudoku size is 2.");
		}

		n = aNValue;
		matrix = new Cell[n * n][n * n];

		for (int i = 0; i < n * n; i++) {
			for (int j = 0; j < n * n; j++) {
				matrix[i][j] = new Cell();
			}
		}
	}

	public int getSize() {
		return n;
	}

	/**
	 * Calculate the number of cells in this type of Sudoku board.
	 * 
	 * @return The number of cells.
	 */
	public int numCels() {
		return (n * n) * (n * n);
	}

	private void setCell(int i, int j, int v, boolean isFixed) {
		if (!isCellPositionValid(i, j)) {
			throw new RuntimeException("Invalid cell position.");
		}

		if (!isCellValueValid(v)) {
			throw new RuntimeException("Invalid cell value.");
		}

		matrix[i][j].setValue(v);
		if (isFixed) {
			matrix[i][j].fix();
		}
	}

	/**
	 * Set a free cell, that is, a cell that can have its value modified.
	 * 
	 * @param i
	 *            The row of the cell.
	 * @param j
	 *            The column of the cell.
	 * @param v
	 *            The value of the cell.
	 */
	public void setFreeCell(int i, int j, int v) {
		setCell(i, j, v, false);
	}

	/**
	 * Set a fixed cell, that is, a cell that can't have its value modified.
	 * It's the cell values that were created with the instance.
	 * 
	 * @param i
	 *            The row of the cell.
	 * @param j
	 *            The column of the cell.
	 * @param v
	 *            The value of the cell.
	 */
	public void setFixedCell(int i, int j, int v) {
		setCell(i, j, v, true);
	}

	/**
	 * Return the cell of the position (i,j).
	 * 
	 * @param i
	 *            The row index [0; (n*n-1)]
	 * @param j
	 *            The column index [0; (n*n-1)]
	 * @return The cell.
	 */
	public Cell getCell(int i, int j) {
		if (!isCellPositionValid(i, j)) {
			throw new RuntimeException("Invalid cell position.");
		}

		return matrix[i][j];
	}

	/**
	 * Check if the given position is inside the board.
	 * 
	 * @param i
	 *            The row index [0; (n*n-1)]
	 * @param j
	 *            The column index [0; (n*n-1)]
	 * @return true if the cell position is valid
	 */
	public boolean isCellPositionValid(int i, int j) {
		return i >= 0 && i < n * n && j >= 0 && j < n * n;
	}

	/**
	 * Checks if the value is valid for this Sudoku size.
	 * 
	 * @param v
	 *            The value [-inf; n*n]
	 * @return
	 */
	public boolean isCellValueValid(int v) {
		return v <= n * n;
	}

	/**
	 * Calculate the cost of the current instance. If the instance is solved,
	 * the cost is zero.
	 * 
	 * @return The cost of the instance.
	 */
	public int cost() {
		int value = 0;

		// cost = number of missing values in rows and in columns
		// Based on (Lewis, Rhyd, 2009)
		for (int i = 0; i < n * n; i++) {

			// creates the lists of required numbers
			List<Integer> neededValuesInRow = new ArrayList<Integer>();
			List<Integer> neededValuesInColumn = new ArrayList<Integer>();
			for (int k = 1; k <= n * n; k++) {
				neededValuesInRow.add(k);
				neededValuesInColumn.add(k);
			}

			for (int j = 0; j < n * n; j++) {
				// check the rows
				if (!matrix[i][j].isEmpty()) {
					if (neededValuesInRow.contains(matrix[i][j].getValue())) {
						neededValuesInRow.remove(neededValuesInRow
								.indexOf(matrix[i][j].getValue()));
					}
				}

				// check the columns
				if (!matrix[j][i].isEmpty()) {
					if (neededValuesInColumn.contains(matrix[j][i].getValue())) {
						neededValuesInColumn.remove(neededValuesInColumn
								.indexOf(matrix[j][i].getValue()));
					}
				}
			}

			// add to the cost value the number of missing numbers in rows and
			// columns
			value += neededValuesInRow.size();
			value += neededValuesInColumn.size();
		}

		return value;
	}

	/**
	 * checks if the cell in the passed position is empty
	 * 
	 * @param i
	 *            the row
	 * @param j
	 *            the column
	 * @return true if the cell is empty (has a value <= 0)
	 */
	public boolean isCellEmpty(int i, int j) {
		return matrix[i][j].isEmpty();
	}

	/**
	 * checks if the instance is valid, i.e., it respects the 3 laws of sudoku:
	 * no row, no column and no box (n*n frames) will have repeated values
	 * 
	 * @return true if the instance is valid
	 */
	public boolean isValid() {
		// stores whether the value k was found in the row/column or not
		ArrayList<Boolean> rowSerie = new ArrayList<Boolean>();
		ArrayList<Boolean> columnSerie = new ArrayList<Boolean>();

		// verify lines
		for (int i = 0; i < n * n; i++) {

			// initializes the series values
			rowSerie.clear();
			columnSerie.clear();
			for (int k = 0; k < n * n; k++) {
				rowSerie.add(false);
				columnSerie.add(false);
			}

			for (int j = 0; j < n * n; j++) {
				if (!isCellEmpty(i, j)) {
					// checks if the value was already in the row
					if (rowSerie.get(matrix[i][j].getValue() - 1)) {
						return false;
					} else {
						rowSerie.set(matrix[i][j].getValue() - 1, true);
					}
				}

				if (!isCellEmpty(j, i)) {
					// checks if the value was already in the column
					if (columnSerie.get(matrix[j][i].getValue() - 1)) {
						return false;
					} else {
						columnSerie.set(matrix[j][i].getValue() - 1, true);
					}
				}
			}
		}
		
		// checks the groups
		ArrayList<Boolean> foundInSubgroup = new ArrayList<Boolean>();
		for (int i = 0; i < n * n; i += n) {
			for (int j = 0; j < n * n; j += n) {

				foundInSubgroup.clear();
				for (int f = 0; f < n * n; f++) {
					foundInSubgroup.add(false);
				}

				// search in the subgroup
				for (int k = 0; k < n; k++) {
					for (int l = 0; l < n; l++) {
						if (!isCellEmpty(i + k, j + l)) {
							if (foundInSubgroup.get(matrix[i + k][j + l]
									.getValue() - 1)) {
								return false;
							} else {
								foundInSubgroup.set(
										matrix[i + k][j + l].getValue() - 1,
										true);
							}
						}
					}
				}
			}
		}

		return true;
	}

	/**
	 * Verifies if the current instance is solved.
	 * 
	 * @return true if it is solved
	 */
	public boolean isSolved() {

		// verify if it has empty fields
		for (int i = 0; i < n * n; i++) {
			for (int j = 0; j < n * n; j++) {
				if (isCellEmpty(i, j)) {
					return false;
				}
			}
		}

		// check if the solution is valid
		if (!isValid()) {
			return false;
		}

		return true;
	}

	/**
	 * Fulfill the blank values generating a (probably) invalid answer.
	 */
	public void fill() {

		// fill the empty spaces in each (n x n) box with the remaining numbers
		// Based on (Lewis, Rhyd, 2009)
		for (int i = 0; i < n * n; i += n) {
			for (int j = 0; j < n * n; j += n) {

				// initialize the list of possible values [1; n*n]
				ArrayList<Integer> possibleValues = new ArrayList<Integer>();
				for (int k = 1; k <= n * n; k++) {
					possibleValues.add(k);
				}

				// remove from the possible values the values that are in the
				// box
				for (int k = 0; k < n; k++) {
					for (int l = 0; l < n; l++) {
						if (!matrix[i + k][j + l].isEmpty()) {
							if (possibleValues.contains(matrix[i + k][j + l]
									.getValue())) {
								possibleValues.remove(possibleValues
										.indexOf(matrix[i + k][j + l]
												.getValue()));
							}
						}
					}
				}

				// add the remaining values to the empty cells of the box
				for (int k = 0; k < n; k++) {
					for (int l = 0; l < n; l++) {
						if (matrix[i + k][j + l].isEmpty()) {
							int val = possibleValues.get(0);
							possibleValues.remove(0);
							matrix[i + k][j + l].setValue(val);
						}
					}
				}
			}
		}
	}

	/**
	 * Cleans an instance keeping only the fixed cells.
	 */
	public void clean() {
		for (int i = 0; i < n * n; i++) {
			for (int j = 0; j < n * n; j++) {
				if (!getCell(i, j).isFixed()) {
					setFreeCell(i, j, -1);
				}
			}
		}
	}

	/**
	 * Generates the neighbourhood of the current instance.
	 * 
	 * @return A list of neighbours of this instance.
	 */
	public Set<Instance> neighbourhood() {
		Set<Instance> neighbourhood = new HashSet<>();

		for (int i = 0; i < n * n; i += n) {
			for (int j = 0; j < n * n; j += n) {
				List<Integer[]> nonFixedCells = new ArrayList<Integer[]>();

				// create a list of non fixed cells in the square
				for (int k = 0; k < n; k++) {
					for (int l = 0; l < n; l++) {
						Cell currentCell = matrix[i + k][j + l];
						if (!currentCell.isFixed()) {
							Integer pos[] = { i + k, j + l };
							nonFixedCells.add(pos);
						}
					}
				}

				// adds to the neighbourhood instances with swaped values in
				// the square. Based on (Lewis, Rhyd, 2009)
				for (int k = 0; k < nonFixedCells.size(); k++) {
					for (int l = k+1; l < nonFixedCells.size(); l++) {
						if (k != l) {
							Instance neighbour = cloneInstance(this);
							Integer posK[] = nonFixedCells.get(k);
							Integer posL[] = nonFixedCells.get(l);
							Cell cellK = getCell(posK[0], posK[1]);
							Cell cellL = getCell(posL[0], posL[1]);

							neighbour.setFreeCell(posK[0], posK[1],
									cellL.getValue());
							neighbour.setFreeCell(posL[0], posL[1],
									cellK.getValue());

							neighbourhood.add(neighbour);
						}
					}
				}
			}
		}

		return neighbourhood;
	}

	/**
	 * Copy a instance
	 * 
	 * @param anInstance
	 *            The instance to be copied
	 * @return The copied instance
	 */
	public Instance cloneInstance(Instance anInstance) {
		Instance copy = new Instance(anInstance.getSize());

		for (int i = 0; i < n * n; i++) {
			for (int j = 0; j < n * n; j++) {
				if (anInstance.getCell(i, j).isFixed()) {
					copy.setFixedCell(i, j, anInstance.getCell(i, j).getValue());
				} else {
					copy.setFreeCell(i, j, anInstance.getCell(i, j).getValue());
				}
			}
		}

		return copy;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (!(other instanceof Instance))
			return false;

		Instance otherInstance = (Instance) other;

		if (n != otherInstance.getSize()) {
			return false;
		}
		
		// checks if the cells are equals
		for (int i = 0; i < n * n; i++) {
			for (int j = 0; j < n * n; j++) {
				if (!getCell(i, j).equals(otherInstance.getCell(i, j))) {
					return false;
				}
			}
		}

		return true;
	}

	@Override
	public String toString() {
		String str = "";
		
		str += "size = " + n + "\n";

		for (int i = 0; i < n * n; i++) {
			for (int j = 0; j < n * n; j++) {
				str += " " + getCell(i, j).getValue();
			}
			str += "\n";
		}
		
		return str;
	}

	@Override
	public int hashCode() {
		int result = 7;
		int c = n + Arrays.deepHashCode(matrix);
		return 43 * result + c;
	}
}
