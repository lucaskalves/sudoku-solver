package sudoku_solver;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class InstanceFileLoaderTest {

	@Test
	public void testLoader() throws IOException {
		String filepath = getClass().getResource("/3_10").getFile();
		InstanceFileLoader loader = new InstanceFileLoader(filepath);

		int instanceDef[][] =  {{5, -1, -1, -1, -1, -1, -1, 4, -1 },
				{7, -1, -1, 8, -1, -1, -1, -1, -1 },
				{-1, -1, -1, -1, -1, -1, -1, -1, -1 },
				{-1, -1, -1, -1, -1, -1, -1, -1, -1 },
				{-1, -1, -1, -1, -1, -1, -1, -1, -1 },
				{-1, -1, -1, -1, -1, -1, -1, -1, -1 },
				{-1, -1, -1, 3, -1, -1, -1, -1, -1 },
				{2, -1, -1, -1, -1, -1, -1, 1, -1 },
				{-1, -1, -1, -1, 2, -1, -1, -1, -1 }};
		
		Instance expectedInstance = new Instance(3);
		
		for(int i=0; i<9; i++) {
			for(int j=0; j<9; j++) {
				int value = instanceDef[i][j];
				if(value>0) {
					expectedInstance.setFixedCell(i, j, value);
				} else {
					expectedInstance.setFreeCell(i, j, value);
				}
			}
		}
		
		assertEquals(expectedInstance, loader.getInstance());
	}

}
