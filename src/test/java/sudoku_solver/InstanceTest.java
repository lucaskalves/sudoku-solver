package sudoku_solver;

import static org.junit.Assert.*;
import sudoku_solver.Instance;

import org.junit.Test;

public class InstanceTest {
	int solvedValidInstance[][] = {{4,2,9,3,1,6,5,7,8},
								   {8,6,7,5,2,4,1,9,3},
								   {5,1,3,8,9,7,2,4,6},
								   {9,3,1,7,8,5,6,2,4},
								   {6,8,2,9,4,1,7,3,5},
								   {7,4,5,2,6,3,9,8,1},
								   {3,5,4,6,7,2,8,1,9},
								   {1,7,8,4,5,9,3,6,2},
								   {2,9,6,1,3,8,4,5,7}};
	
	int almostEqual[][] =  {{4,2,9,3,1,6,5,7,8},
			   {8,6,7,5,2,4,1,9,3},
			   {5,1,3,8,9,7,2,4,6},
			   {9,3,1,7,8,5,6,2,4},
			   {6,8,2,9,4,1,7,3,5},
			   {7,4,5,2,6,3,9,8,1},
			   {3,5,4,6,7,2,8,1,9},
			   {1,7,8,4,5,9,3,6,7},
			   {2,9,6,1,3,8,4,5,2}};
	
	int validIncompleteInstance[][]= {{4,2,9,3,1,6,5,7,8},
								      {8,6,7,5,2,4,1,9,3},
								      {5,1,3,8,9,7,2,4,6},
								      {9,3,1,7,8,5,6,2,4},
								      {6,8,2,9,4,1,7,3,5},
								      {7,4,5,2,6,3,9,8,1},
								      {3,5,4,6,7,2,8,1,9},
								      {1,7,8,4,5,9,3,6,2},
								      {2,9,6,1,3,8,4,5,-1}};
	
	int invalidIncompleteInstance[][]= {{4,4,9,3,1,6,5,7,8},
									    {8,6,7,5,2,4,1,9,3},
									    {5,1,3,8,9,7,-1,4,6},
									    {9,3,1,7,8,-1,6,2,4},
							  		    {6,8,2,9,4,1,7,3,5},
									    {7,4,5,2,-1,3,9,8,1},
									    {3,5,4,6,-1,2,8,1,9},
									    {1,7,8,4,5,9,3,6,2},
									    {2,9,6,1,3,8,4,5,-1}};
	
	int boxInvalidInstance[][] = {{1,2,3,4,5,6,7,8,9},
							      {2,3,4,5,6,7,8,9,1},
							      {3,4,5,6,7,8,9,1,2},
							      {4,5,6,7,8,9,1,2,3},
							      {5,6,7,8,9,1,2,3,4},
							      {6,7,8,9,1,2,3,4,5},
							      {7,8,9,1,2,3,4,5,6},
							      {8,9,1,2,3,4,5,6,7},
							      {9,1,2,3,4,5,6,7,8}};
	
	int rowInvalidInstance[][] = {{4,2,9,3,1,6,5,7,8},
							      {8,6,7,5,2,4,1,9,3},
							      {5,1,1,8,9,7,2,4,6},
							      {9,3,1,7,8,5,6,2,4},
							      {6,8,2,9,4,1,7,3,5},
							      {7,4,5,2,6,3,9,8,1},
							      {3,5,4,6,7,2,8,1,9},
							      {1,7,8,4,5,9,3,6,2},
							      {2,9,6,1,3,8,4,5,7}};
	
	int colInvalidInstance[][] = {{4,2,9,3,1,6,5,7,8},
							       {8,6,7,5,2,4,1,9,3},
							       {5,1,3,8,9,7,2,4,6},
							       {9,3,1,7,8,5,6,2,4},
							       {6,8,2,9,4,1,7,3,4},
							       {7,4,5,2,6,3,9,8,1},
							       {3,5,4,6,7,2,8,1,9},
							       {1,7,8,4,5,9,3,6,2},
							       {2,9,6,1,3,8,4,5,7}};


	@Test
	public void testConstruction() {
		Instance instance3 = new Instance(3);
		Instance instance4 = new Instance(4);

		assertEquals(81, instance3.numCels());
		assertEquals(256, instance4.numCels());
	}
	
	@Test
	public void testSetFreeCell() {
		Instance instance3 = new Instance(3);
		instance3.setFreeCell(1, 1, 5);
		instance3.setFreeCell(1, 2, 7);

		assertEquals(new Cell(5, false), instance3.getCell(1, 1));
		assertEquals(new Cell(7, false), instance3.getCell(1, 2));
	}
	
	@Test
	public void testSetFixedCell() {
		Instance instance3 = new Instance(3);
		instance3.setFixedCell(1, 1, 5);
		instance3.setFixedCell(1, 2, 7);

		assertEquals(new Cell(5, true), instance3.getCell(1, 1));
		assertEquals(new Cell(7, true), instance3.getCell(1, 2));
	}
	
	@Test
	public void testValidCellPosition() {
		Instance instance3 = new Instance(3);

		assertTrue(instance3.isCellPositionValid(0, 0));
		assertTrue(instance3.isCellPositionValid(1, 1));
		assertTrue(instance3.isCellPositionValid(0, 1));
		assertTrue(instance3.isCellPositionValid(1, 0));
		assertTrue(instance3.isCellPositionValid(8, 8));
		assertTrue(instance3.isCellPositionValid(0, 8));
		assertTrue(instance3.isCellPositionValid(8, 0));
		assertFalse(instance3.isCellPositionValid(-1, -1));
		assertFalse(instance3.isCellPositionValid(0, -1));
		assertFalse(instance3.isCellPositionValid(-1, 0));
		assertFalse(instance3.isCellPositionValid(9, 9));
		assertFalse(instance3.isCellPositionValid(0, 9));
		assertFalse(instance3.isCellPositionValid(9, 0));
	}
	
	@Test
	public void testValidCellValue() {
		Instance instance3 = new Instance(3);

		assertTrue(instance3.isCellValueValid(1));
		assertTrue(instance3.isCellValueValid(5));
		assertTrue(instance3.isCellValueValid(9));
		assertTrue(instance3.isCellValueValid(0));
		assertTrue(instance3.isCellValueValid(-1));
		assertFalse(instance3.isCellValueValid(10));
		assertFalse(instance3.isCellValueValid(15));
	}
	
	@Test
	public void testIsValid() {
		Instance solvedValid = new Instance(3);
		Instance validIncomplete = new Instance(3);
		Instance invalidIncomplete = new Instance(3);
		Instance boxInvalid = new Instance(3);
		Instance rowInvalid = new Instance(3);
		Instance colInvalid = new Instance(3);
		
		for(int i=0; i<9; i++) {
			for(int j=0; j<9; j++) {
				solvedValid.setFreeCell(i, j, solvedValidInstance[i][j]);
				validIncomplete.setFreeCell(i, j, validIncompleteInstance[i][j]);
				invalidIncomplete.setFreeCell(i, j, invalidIncompleteInstance[i][j]);
				boxInvalid.setFreeCell(i, j, boxInvalidInstance[i][j]);
				rowInvalid.setFreeCell(i, j, rowInvalidInstance[i][j]);
				colInvalid.setFreeCell(i, j, colInvalidInstance[i][j]);
			}
		}

		assertTrue(solvedValid.isValid());
		assertTrue(validIncomplete.isValid());
		assertFalse(invalidIncomplete.isValid());
		assertFalse(boxInvalid.isValid());
		assertFalse(rowInvalid.isValid());
		assertFalse(colInvalid.isValid());
	}
	
	@Test
	public void testIsSolved() {
		Instance solvedValid = new Instance(3);
		Instance validIncomplete = new Instance(3);
		Instance invalidIncomplete = new Instance(3);
		Instance boxInvalid = new Instance(3);
		Instance rowInvalid = new Instance(3);
		Instance colInvalid = new Instance(3);
		
		for(int i=0; i<9; i++) {
			for(int j=0; j<9; j++) {
				solvedValid.setFreeCell(i, j, solvedValidInstance[i][j]);
				validIncomplete.setFreeCell(i, j, validIncompleteInstance[i][j]);
				invalidIncomplete.setFreeCell(i, j, invalidIncompleteInstance[i][j]);
				boxInvalid.setFreeCell(i, j, boxInvalidInstance[i][j]);
				rowInvalid.setFreeCell(i, j, rowInvalidInstance[i][j]);
				colInvalid.setFreeCell(i, j, colInvalidInstance[i][j]);
			}
		}

		assertTrue(solvedValid.isSolved());
		assertFalse(validIncomplete.isSolved());
		assertFalse(invalidIncomplete.isSolved());
		assertFalse(boxInvalid.isSolved());
		assertFalse(rowInvalid.isSolved());
		assertFalse(colInvalid.isSolved());
	}
	@Test
	public void testHashcode() {
		Instance instance1 = new Instance(3);
		Instance instance2 = new Instance(3);
		Instance instance3 = new Instance(3);
		Instance instance4 = new Instance(3);
		
		for(int i=0; i<9; i++) {
			for(int j=0; j<9; j++) {
				instance1.setFreeCell(i, j, solvedValidInstance[i][j]);
				instance2.setFreeCell(i, j, solvedValidInstance[i][j]);
				instance3.setFreeCell(i, j, almostEqual[i][j]);
				instance4.setFreeCell(i, j, invalidIncompleteInstance[i][j]);
			}
		}

		assertEquals(instance1.hashCode(), instance2.hashCode());
		assertNotEquals(instance3.hashCode(), instance1.hashCode());
		assertNotEquals(instance4.hashCode(), instance1.hashCode());
	}
	@Test
	public void testCost() {
		Instance solvedValid = new Instance(3);
		Instance incomplete = new Instance(3);
		
		int incompleteInstance[][] = {{4,-1,-1,3,1,6,5,7,-1},
									  {8,6,7,5,2,4,1,9,3},
									  {5,1,3,8,9,7,2,4,6},
									  {9,3,1,7,8,5,6,2,4},
									  {6,8,2,9,4,1,7,3,5},
									  {7,4,5,2,6,-1,9,8,1},
									  {3,5,4,6,7,2,8,1,9},
									  {1,7,8,4,5,9,3,6,2},
									  {2,9,6,1,-1,8,4,5,-1}};
		
		for(int i=0; i<9; i++) {
			for(int j=0; j<9; j++) {
				solvedValid.setFreeCell(i, j, solvedValidInstance[i][j]);
				incomplete.setFreeCell(i, j, incompleteInstance[i][j]);
			}
		}

		assertEquals(0, solvedValid.cost());
		assertEquals(12, incomplete.cost());
	}
	
	@Test
	public void testFill() {
		Instance instance = new Instance(3);

		int incompleteInstance[][] = {{4,-1,-1,3,1,6,5,7,-1},
									  {8,-1,-1,5,2,4,1,9,3},
									  {5,1,-1,8,9,7,2,4,6},
									  {9,3,1,7,8,5,6,2,4},
									  {6,8,2,9,4,1,7,3,5},
									  {7,4,5,2,6,-1,9,8,1},
									  {3,-1,4,6,7,2,8,1,9},
									  {1,-1,8,4,-1,9,3,6,2},
									  {2,9,6,1,-1,8,4,5,-1}};
		
		for(int i=0; i<9; i++) {
			for(int j=0; j<9; j++) {
				instance.setFreeCell(i, j, incompleteInstance[i][j]);
			}
		}
		
		instance.fill();
		
		for(int i=0; i<9; i++) {
			for(int j=0; j<9; j++) {
				assertFalse(instance.isCellEmpty(i, j));
			}
		}
	}
}
