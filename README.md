Sudoku Solver
=============

A great Sudoku Solver using GRASP (Greedy Randomized Adaptive Search Procedure)
algorithm.

## Requirements  ##

* Java JDK 1.7
* Maven 3

## Compilation  ##

This application is implemented with Java and uses Maven to control
the dependencies and the build steps. To generate a *jar* file, 
run the following command:

    mvn clean compile assembly:single

A file called *sudoku-solver-0.0.1-SNAPSHOT-jar-with-dependencies.jar* 
will be generated at *target* folder.

## Usage  ##

     java -jar ./target/sudoku-solver-0.0.1-SNAPSHOT-jar-with-dependencies.jar
     <instance-filepath> [-g grasp-max-iterations]
     [-a grasp-alpha] [-l local-search-max-iterations]    

This is a sample call to solve 3\_80 instance.

     java -jar ./target/sudoku-solver-0.0.1-SNAPSHOT-jar-with-dependencies.jar ./instances/3_80
       
### Options ###

**instance-filepath**: filepath to the sudoku instance file     
**-g**: max number of iterations GRASP will run. Default = 1000    
**-a**: alpha value for GRASP. Default = 0.2    
**-l**: max number of iterations local search will run. Default = 2000    

## Sample Data  ##

The folder *instances* hold some sudoku problems representations. You can
use them as instance-filepath in the command line. The name of the instances
are as follow: x\_y, x = size of the instance (e.g.: x=3: a default 9x9 sudoku
matrix), y = per centage of filled cells in the instance.

The instance 3\_26 is claimed to be the world's hardest (3x3) 
sudoku instance. More information can be found [here](http://www.telegraph.co.uk/science/science-news/9359579/Worlds-hardest-sudoku-can-you-crack-it.html).
 I don't know if it really is, but it's pretty hard :P.

## License  ##

The good and old MIT License. See the license.txt file in the root folder.

## Authors ##
[Adolfo Schneider](http://twitter.com/adolfosrs)    
[Lucas Kreutz](http://lucaskreutz.com.br)
